//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Volumes/DATA/Android/sample-java-j2objc/shared/src/main/java/com/mysoftsource/shared/IOSPlatform.java
//

#include "IOSPlatform.h"
#include "J2ObjC_source.h"

@implementation TBPIOSPlatform

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  TBPIOSPlatform_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (NSString *)getPlatformName {
  return @"This is iOS";
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(getPlatformName);
  #pragma clang diagnostic pop
  static const J2ObjcClassInfo _TBPIOSPlatform = { "IOSPlatform", "com.mysoftsource.shared", NULL, methods, NULL, 7, 0x1, 2, 0, -1, -1, -1, -1, -1 };
  return &_TBPIOSPlatform;
}

@end

void TBPIOSPlatform_init(TBPIOSPlatform *self) {
  NSObject_init(self);
}

TBPIOSPlatform *new_TBPIOSPlatform_init() {
  J2OBJC_NEW_IMPL(TBPIOSPlatform, init)
}

TBPIOSPlatform *create_TBPIOSPlatform_init() {
  J2OBJC_CREATE_IMPL(TBPIOSPlatform, init)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(TBPIOSPlatform)
