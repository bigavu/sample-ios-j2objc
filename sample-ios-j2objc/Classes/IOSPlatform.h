//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Volumes/DATA/Android/sample-java-j2objc/shared/src/main/java/com/mysoftsource/shared/IOSPlatform.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IOSPlatform")
#ifdef RESTRICT_IOSPlatform
#define INCLUDE_ALL_IOSPlatform 0
#else
#define INCLUDE_ALL_IOSPlatform 1
#endif
#undef RESTRICT_IOSPlatform

#if !defined (TBPIOSPlatform_) && (INCLUDE_ALL_IOSPlatform || defined(INCLUDE_TBPIOSPlatform))
#define TBPIOSPlatform_

#define RESTRICT_MobilePlatform 1
#define INCLUDE_TBPMobilePlatform 1
#include "MobilePlatform.h"

@interface TBPIOSPlatform : NSObject < TBPMobilePlatform >

#pragma mark Public

- (instancetype)init;

- (NSString *)getPlatformName;

@end

J2OBJC_EMPTY_STATIC_INIT(TBPIOSPlatform)

FOUNDATION_EXPORT void TBPIOSPlatform_init(TBPIOSPlatform *self);

FOUNDATION_EXPORT TBPIOSPlatform *new_TBPIOSPlatform_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT TBPIOSPlatform *create_TBPIOSPlatform_init(void);

J2OBJC_TYPE_LITERAL_HEADER(TBPIOSPlatform)

@compatibility_alias ComMysoftsourceSharedIOSPlatform TBPIOSPlatform;

#endif

#pragma pop_macro("INCLUDE_ALL_IOSPlatform")
