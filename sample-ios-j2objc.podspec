#
# Be sure to run `pod lib lint sample-ios-j2objc.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'sample-ios-j2objc'
  s.version          = '1.0.7'
  s.summary          = 'This is pod repository for j2objc sample'
  s.xcconfig = { 'J2OBJC_HOME' => '/Volumes/DATA/Tool/ConvertAndroidToiOS/j2objc-2.0.5' }
  s.xcconfig = { 'FRAMEWORK_SEARCH_PATHS' => '${J2OBJC_HOME}/frameworks' }
  s.xcconfig = { 'LIBRARY_SEARCH_PATHS' => '${J2OBJC_HOME}/lib' }
  s.xcconfig = { 'USER_HEADER_SEARCH_PATHS' => '${J2OBJC_HOME}/include' }

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/bigavu/sample-ios-j2objc'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'hoanglm4' => 'minhhoangtoday@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/bigavu/sample-ios-j2objc.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'sample-ios-j2objc/Classes/**/*'
  
  # s.resource_bundles = {
  #   'sample-ios-j2objc' => ['sample-ios-j2objc/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
