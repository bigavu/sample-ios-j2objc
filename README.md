# sample-ios-j2objc

[![CI Status](http://img.shields.io/travis/hoanglm4/sample-ios-j2objc.svg?style=flat)](https://travis-ci.org/hoanglm4/sample-ios-j2objc)
[![Version](https://img.shields.io/cocoapods/v/sample-ios-j2objc.svg?style=flat)](http://cocoapods.org/pods/sample-ios-j2objc)
[![License](https://img.shields.io/cocoapods/l/sample-ios-j2objc.svg?style=flat)](http://cocoapods.org/pods/sample-ios-j2objc)
[![Platform](https://img.shields.io/cocoapods/p/sample-ios-j2objc.svg?style=flat)](http://cocoapods.org/pods/sample-ios-j2objc)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

sample-ios-j2objc is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'sample-ios-j2objc'
```

## Author

hoanglm4, minhhoangtoday@gmail.com

## License

sample-ios-j2objc is available under the MIT license. See the LICENSE file for more info.
