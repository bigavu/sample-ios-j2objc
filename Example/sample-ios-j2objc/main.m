//
//  main.m
//  sample-ios-j2objc
//
//  Created by hoanglm4 on 02/09/2018.
//  Copyright (c) 2018 hoanglm4. All rights reserved.
//

@import UIKit;
#import "TBPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TBPAppDelegate class]));
    }
}
